# Memory tester

Objective of this mini-project is to play around non-volatile memories in different microcontrollers/dedicated units and see the difference in performance. Non-volatile memories in modern microcontrollers have different characteristics, and it is always good to know what exact microcontroller is capable of. Datasheet information is a useful source of knowledge, but only practice is the criteria of truth.

### Requirements:

* develop interface for accessing non-volatile memory for the test. Implementation of this interface should afterwards be used in the test rack.

* develop test application, capable of several testing modes;
    * measure operations' timings: write time, erase time, read time;
	* perform durability testing: run cycle erase-write-read cycle many times and register: moment of the first error, moment of first 10 errors, moment of 100 errors, and so on. Also define the type of the errors: is it erros from the controller (error flags) or read-write mismatch errors. Types of the tests: write pseudo-random data, write only 1-s (or only 0-s);
	* during durability testing stop every N cycles and perform timing analysis on the area under test;

* provide output of the test in easy-to-parse format via UART interface;

* develop Python application for analysis and storage of test results;

* develop a test rack for applying different external conditions of work (Pelte's element for cooling the DUT down, heater for heating DUT up), controllable from outside. 

* testbench should be very easy to reproduce for other users that may want to repeat the result.

* test following targets: Arduino flash memory, STM8, MSP430 (Flash and FRAM versions), STM32, MSP432, NRF52, SPC56, S32K, Infineon, Winbond NOR, Micron NOR, some NAND chips, FRAM, MRAM (external chips0.